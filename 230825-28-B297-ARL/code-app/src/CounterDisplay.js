import { } from 'react-bootstrap';

function CounterDisplay({counterProp}) {
    const count = counterProp;
    return (
        <div className='d-flex justify-content-center m-3 text-light h1'>Counter: {count}</div>
    )
}

export default CounterDisplay;