import './App.css';
import Counter from './Counter';
import { Container, Row, Col } from 'react-bootstrap';

function App() {
  return (
    <div className="App bg-dark">
      <Container className="d-flex justify-content-center align-items-center vh-100">
        <Row>
          <Col>
            <Counter />
          </Col>
        </Row>
      </Container>
      
    </div>
  );
}

export default App;
