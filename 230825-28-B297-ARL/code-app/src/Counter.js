import CounterDisplay from "./CounterDisplay";
import { useState } from "react";
import { Button } from 'react-bootstrap';


function Counter() {
    const [count, setCount] = useState(0);

    const increment = () => {
        setCount(count + 1);
    }

    const decrement = () => {
        setCount(count - 1);
    }

    return (
        <div>
            <CounterDisplay counterProp={count} />
            <Button variant="success m-2" onClick={increment}>Increment</Button>
            <Button variant="danger m-2" onClick={decrement}>Decrement</Button>
        </div>
        
    )
}


export default Counter;