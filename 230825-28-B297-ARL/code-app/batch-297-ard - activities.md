# 230825/230828 - Batch 297:  Academic Reading List 

### **Topics**

- React JS Resources
  - [Create a New React App](https://reactjs.org/docs/create-a-new-react-app.html)
  - [Virtual DOM](https://www.youtube.com/watch?v=M-Aw4p0pWwg)
  - [Introducing JSX](https://reactjs.org/docs/introducing-jsx.html)
  - [JSX Fragment](https://legacy.reactjs.org/docs/fragments.html#short-syntax)
  - [React-Bootstrap Documentation](https://react-bootstrap.github.io/)
  - [React JS Components and Props](https://reactjs.org/docs/components-and-props.html)
- React JS Props
  - [resources](https://reactjs.org/docs/components-and-props.html)
- Props Drilling
  - [resources](https://medium.com/front-end-weekly/props-drilling-in-react-js-723be80a08e5)
- Lists and Keys
  - [resources](https://reactjs.org/docs/lists-and-keys.html)
- Typechecking With PropTypes
  - [resources](https://reactjs.org/docs/typechecking-with-proptypes.html)
- React JS States
  - [resources](https://reactjs.org/docs/hooks-state.html;)

Advance Reading:
- React JS useEffect Hook
  - [resources](https://reactjs.org/docs/hooks-effect.html#gatsby-focus-wrapper)
- React-Bootstrap Form Component
  - [resources](https://react-bootstrap.github.io/components/forms/)
- React JS Events
  - [resources](https://reactjs.org/docs/events.html)



### **Purpose**
- Review and practice creating a React project, creating components, add React States, add props passing to those components.
- Create new react application from scratch with create-react-app with react and react-bootstrap for styling.
- Practice on creating states in React and pass data between parent and child components using props by following the steps in making React projects such as Counter App and Todo List App


### **Goal to Checking**

#### In our batch hangouts, add the Gitlab repo link and a screenshot of your output to the thread named "230825/230828 Academic Reading List Output":

### Activity 1: Counter App

1. Create a new React application using `npx create-react-app counter-app`.

2. Clean the project folder by deleting the unused files
    - App.test.js
    - index.css
    - logo.svg
    - reportWebVitals.js
    - setupTests.js

3. Remove the importation of the "index.css" and "reportWebVitals" files from the "index.js" file. Also remove the code using the reportWebVitals function. And, Remove the importation of the "logo.svg" file and most of the JSX codes found inside `return` of the "App" component leaving only a fragment `<></>` to remove any errors.

3. Inside the `src` folder, create three new files: `Counter.js`, and `CounterDisplay.js`.

4. **Counter.js:**
   - Import the necessary React module (if needed).
   - Import the `CounterDisplay` component.
   - Create a functional component named `Counter`.
   - Use the `useState` hook to initialize a `count` state variable with an initial value of `0`.
   - Create two functions, `increment` and `decrement`, that modify the `count` state variable when the corresponding buttons are clicked.
   - Render the `CounterDisplay` component, passing the `count` state as a prop.
   - Render buttons for incrementing and decrementing the count.

5. **CounterDisplay.js:**
   - Import the necessary React module (if needed).
   - Create a functional component named `CounterDisplay` that accepts a prop named `count`.
   - Return a JSX element that displays the provided `count` prop.

6. **App.js:**
   - Import the necessary React module and your `Counter` component.
   - Create a functional component named `App`.
   - Inside the `App` component, render the `Counter` component.

7. Style the components (optional):
   - You can add basic styling to your components using CSS classes or using Bootstrap and react-bootstrap.

8. Run the application using `npm start`.
   - Open your browser and navigate to `http://localhost:3000`.
   - You should see the Counter App with the initial count value displayed and buttons to increment and decrement the count.

9. Test the application:
   - Click the "Increment" button and observe the count increasing.
   - Click the "Decrement" button and observe the count decreasing.
   - Ensure that the `CounterDisplay` component correctly displays the count passed from the `Counter` component.

**Summary:**
In this activity, you will create a Counter App in React. You'll learn how to manage state using the `useState` hook and pass state data between components using props. This will give you hands-on experience in handling basic state management and prop drilling in React.


### Activity 2: Todo List Application

#### Step 1: Setup
1. Create a new React application using `npx create-react-app todo-app`.

2. Clean the project folder by deleting the unused files
    - App.test.js
    - index.css
    - logo.svg
    - reportWebVitals.js
    - setupTests.js

3. Remove the importation of the "index.css" and "reportWebVitals" files from the "index.js" file. Also remove the code using the reportWebVitals function. And, Remove the importation of the "logo.svg" file and most of the JSX codes found inside `return` of the "App" component leaving only a fragment `<></>` to remove any errors.

#### Step 2: Create Components
1. Inside the `src` folder of your project, create three new files: `TodoList.js`, and `TodoItem.js`.

#### Step 3: Implement `TodoItem.js`
1. In `TodoItem.js`, create a functional component named `TodoItem`.
2. This component should accept two props: `todo` (containing the task details) and `onDelete` (a function to delete the task).
3. Render a `<div>` element with the task text and a "Delete" button.
4. Attach the `onDelete` function to the "Delete" button's click event, passing the `todo.id` to it.

#### Step 4: Implement `TodoList.js`
1. In `TodoList.js`, create a functional component named `TodoList`.
2. This component should accept a prop named `todos` (an array of tasks) and `onDelete` (the delete function).
3. Inside this component, map over the `todos` array and render a `TodoItem` component for each task.
4. Pass the `todo` and `onDelete` props to each `TodoItem` component.

#### Step 5: Implement `App.js`
1. Inside the `App` component, set up a state using the `useState` hook. Name the state variable `todos`.
2. Implement two functions:
   - `addTodo`: Accepts a text input, creates a new task object with a unique `id` and the provided text, and adds it to the `todos` state.
   - `deleteTodo`: Accepts an `id` and updates the `todos` state by removing the task with the corresponding `id`.
3. Render an `<h1>` heading with "Todo List".
4. Render the `TodoList` component, passing it the `todos` state and the `deleteTodo` function as props.
5. Render an `<input>` element that listens

#### Step 6: Styling (Optional)
1. Update the `App.css` file to add basic styling to your components or use Bootstrap and react-bootstrap.

#### Step 7: Run the Application
1. In the terminal, navigate to your project folder.
2. Run `npm start` to start the development server and view your Todo List application in your browser.

**Completion:**
Congratulations! You've successfully built a simple Todo List application in React. This activity helped you practice managing state, creating and passing down components, and using props for interactive functionality. You can further enhance the application by adding features like task completion, editing, or data persistence.
