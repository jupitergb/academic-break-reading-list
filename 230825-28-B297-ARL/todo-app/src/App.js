import { useState } from 'react';
import './App.css';
import TodoList from './TodoList'
import { v4 as uuidv4 } from 'uuid';
import Grid from '@mui/material/Unstable_Grid2';
import { Paper } from '@mui/material';
import Input from '@mui/material/Input';
import { InputAdornment } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';


function App() {

  const [todos, setTodos] = useState([]);


  function addTodo(input) {
    const newTask = {
      id: uuidv4(),
      text: input
    }
    setTodos([...todos, newTask]);
  }  

  function deleteTodo(id) {
    setTodos(todos.filter(todo => id !== todo.id))    
  }

  function getData(input) {
    if(input.key === "Enter" && input.target.value.trim() !== '' ) {
      addTodo(input.target.value)
      input.target.value = "";
    }    
  }

  return (
    <Grid container style={{
          display: 'flex',
          justifyContent: 'center',
          alignContent: 'flex-start',
          minHeight: '100vh',
          background: 'linear-gradient(to bottom, #87CEEB, #2196F3)'}}>
      
      <Grid item xs={12} sx={{display: 'flex', justifyContent: 'center', margin:'50px 0', height:'70px',}}>

          <Paper elevation={24} 
                    sx={{display: 'flex',
                    justifyContent: 'center',
                    paddingTop:'1rem',
                    width:'400px'}}>

          <h1>Todo List</h1>
          </Paper>        
       </Grid>


      <Grid item xs={12} sx={{display: 'flex', justifyContent: 'center', marginBottom: '100px'}}>

        <Paper elevation={24} 
              sx={{display: 'flex',
              justifyContent: 'center',
              paddingTop:'1rem',
              minHeight: '400px',
              width:'400px',
              overflow: 'hidden'}}>
          <Grid>
            <Grid item xs={12}>      
              
            </Grid>
            <Grid item xs={12}>
              <TodoList todos={todos} onDelete={deleteTodo}/>    
              <Input  sx={{width: '20em', margin:'15px 0px 50px 0px'}}
                type="text"
                placeholder='What will you do?'
                onKeyDown={getData}
                inputProps={{ maxLength: 23 }}
                startAdornment={
                  <InputAdornment position="start">                    
                      <AddIcon sx={{color: 'black'}}/>                    
                  </InputAdornment>
                }
              />            
            </Grid>
          </Grid>    
        </Paper>        
      </Grid>
      
    </Grid> 
    
    
  );
}

export default App;
