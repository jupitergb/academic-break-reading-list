import TodoItem from "./TodoItem";
import List from '@mui/material/List';

function TodoList({todos, onDelete}) {

    const todoItem = todos.map(todo => {
        return (
            <TodoItem  key={todo.id} todo={todo} onDelete={onDelete}/>
        )
    })

    
    return (
        <List>
            {todoItem}        
        </List>
    )
}

export default TodoList;