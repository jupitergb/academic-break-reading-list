
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import ListItemIcon from '@mui/material/ListItemIcon';
import AssignmentIcon from '@mui/icons-material/Assignment';

function TodoItem({todo, onDelete}) {
   
    return (
         
        <ListItem disablePadding sx={{ width: '100%', padding: '0 5px' }} secondaryAction={
            <IconButton edge="end" aria-label="delete" onClick={() => onDelete(todo.id)}>
              <DeleteIcon sx={{color: 'red'}}/>
            </IconButton>
          } >
            <ListItemIcon>
                <AssignmentIcon sx={{color: 'black'}} />
            </ListItemIcon>                 
            <ListItemText primary={todo.text} sx={{color: 'black'}}
           />                
        </ListItem>

    )
}

export default TodoItem;